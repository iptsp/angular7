import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service'

@Injectable()
/*
@Injectable({
  providedIn: 'root'
})
*/

export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService, private router: Router) {}
  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigateByUrl('/login');
      return false;
    } return true;
  }

}
