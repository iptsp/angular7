import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
/*
@Injectable({
  providedIn: 'root'
})
*/
export class AuthService {

  constructor(public jwtHelper: JwtHelperService) { }

  public isAuthenticated(): boolean {
    const token  = localStorage.getItem('token');
    const userId = localStorage.getItem('userId');
    if(!token || !userId) {
      return false;
    } return true;
  }

}
