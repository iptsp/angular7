import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs'
import { map, catchError, timeout } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class WebApiService {

  private apiUrl = "http://localhost:3000/api/";

  constructor(private http: HttpClient) {}

  private extractData(res: Response) {
    console.log("Output", res);
    return res;
  }

  getRequest(param) {
    let api = this.apiUrl + param;
    return this.http.get(api).pipe(
      timeout(300000),
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  postRequest(param, data, headers = false) {
    let api = this.apiUrl + param;
    let formData = new FormData();
    for ( let key in data ) {
      let value = data[key];
      if(value == null || !value)
        value = "";
      formData.append(key,value);
    }
    return this.http.post(api, formData).pipe(
      timeout(300000),
      map(this.extractData),
      catchError(this.handleError)
    );

  }

  catchError(error: Response | any) {
    return Observable.throw(error || "Sorry! no internet connection");
  }

  handleError(errorResponse: HttpErrorResponse) {
    if(errorResponse.error instanceof ErrorEvent) {
      console.error("Client side error: " + errorResponse.message);
    } else {
      console.error("Server side error " + errorResponse.message);
    } return throwError("This is a problem with the service. We are notified & working on this.");
  }

}
