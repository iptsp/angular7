import { Component, Input } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  title = 'Post Bill CDR';
  loggedIn = false;

  constructor(public jwtHelper: JwtHelperService, public router: Router) { }

  ngOnInit() {
    if(this.isAuthenticated()) {
      this.loggedIn = true;
    }
  }

  private isAuthenticated(): boolean {
    const token  = localStorage.getItem('token');
    const userId = localStorage.getItem('userId');
    if(!token || !userId) {
      return false;
    } return true;
  }

  public logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    this.loggedIn = false;
    this.router.navigateByUrl('/login');
  }

}
