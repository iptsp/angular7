import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserManageComponent } from './user-manage/user-manage.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ClientProfileComponent } from './client-profile/client-profile.component';
import { PackageComponent } from './package/package.component';

import { AuthGuardService as AuthGuard } from './services/auth/auth-guard.service';
import { ClientComponent } from './client/client.component';
import { ClientAddComponent } from './client-add/client-add.component';

const appRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard],
        
    },
    {
        path: 'user-profile',
        component: UserProfileComponent,
        canActivate: [AuthGuard]

    },
    {
        path: 'client/:id',
        component: ClientProfileComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'users',
        component: UserManageComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'clients',
        component: ClientComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'client-add',
        component: ClientAddComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'packages',
        component: PackageComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'reports',
        component: PackageComponent,
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: ''
    }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

export const RoutingComponents = [
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    UserManageComponent,
    UserProfileComponent,
    ClientProfileComponent,
    PackageComponent,
    ClientComponent,
    ClientAddComponent
];