import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.css']
})
export class PackageComponent implements OnInit {

  lists = [
    {
      name: "Pack 1",
      tariff: 0.60
    },
    {
      name: "Pack 2",
      tariff: 0.90
    }
  ];

  packageForm = null;

  constructor() { }

  ngOnInit() {
    this.packageForm = this.formInit();
  }

  onSubmit() {
    if (this.packageForm.invalid) {
      alert("All Fields are required");
      return;
    }
    let id     = this.packageForm.value.id;
    let name   = this.packageForm.value.name;
    let tariff = this.packageForm.value.tariff;

    if(id != null && this.lists[id]) {
      this.lists[id].name = name;
      this.lists[id].tariff = tariff;
    } else {
      this.lists.push({
        name: name,
        tariff: tariff
      });
    }
    this.packageForm.reset();
    this.ngOnInit();
  }

  onEditItem(index) {
    this.packageForm = new FormGroup({
      id: new FormControl(index, Validators.required),
      name: new FormControl(this.lists[index].name, Validators.required),
      tariff: new FormControl(this.lists[index].tariff, Validators.required),
    });
  }

  onRemoveItem(index) {
    this.lists.splice(index, 1);
  }

  private formInit() {
    return new FormGroup({
      id: new FormControl(''),
      name: new FormControl('', Validators.required),
      tariff: new FormControl('', Validators.required),
    });
  }

}
