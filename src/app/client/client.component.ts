import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  lists = [
    {
      id: "1",
      name: "AAA",
      type: "home",
      mobile: "01711112233",
      email: "email@domain.com"
    }
  ];

  constructor(private router: Router) { }

  ngOnInit() { }

  onRemoveItem(id) {
    //this.lists.splice(id, 1);
    for (var i = 0; i < this.lists.length; i++)
    if (this.lists[i].id === id) { 
        this.lists.splice(i, 1);
        break;
    }
  }

  onViewItem(id) {
    this.router.navigateByUrl('/client/'+id);
  }

}
