import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WebApiService } from '../services/api/web-api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  loading  = false;
  errorMsg;

  @Output() loggedInByLogin = new EventEmitter<boolean>();
  
  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });


  constructor(public router: Router, private api: WebApiService) {
    
  }

  ngOnInit() {    
  }

  onSubmit() {    
    this.errorMsg = false;
    if (this.loginForm.invalid) {
      this.errorMsg= "All field are required";
      return;
    }
    this.loading = true;
    this.api.postRequest("users/login", this.loginForm.value).subscribe(
      (res: any) => {
        this.loading = false;
        if(res.status) {
          localStorage.setItem("token", res.token);
          localStorage.setItem("userId", res.data._id);
          this.loggedInByLogin.emit(true);
          window.location.href = "/dashboard";
        } else {
          this.errorMsg= "Invalid username or password";
        }
        console.log("1111", res);
      }, error => {
        this.loading = false;
        this.errorMsg= "Invalid username or password";
      }
    );  
  }

}
