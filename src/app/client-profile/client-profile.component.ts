import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-client-profile',
  templateUrl: './client-profile.component.html',
  styleUrls: ['./client-profile.component.css']
})
export class ClientProfileComponent implements OnInit {

  id: number;

  item = {
    id: "1",
    name: "AAA",
    type: "home",
    mobile: "01711112233",
    email: "email@domain.com"
  };

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.params.subscribe( params => {
      if(params && params.id) {
        this.id = params.id;
      } else {
        this.router.navigateByUrl('/clients');
      }
    });
  }

  ngOnInit() {
  }

  onEditItem() {

  }

}
