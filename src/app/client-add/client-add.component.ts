import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-client-add',
  templateUrl: './client-add.component.html',
  styleUrls: ['./client-add.component.css']
})
export class ClientAddComponent implements OnInit {

  clientForm = new FormGroup({
    name: new FormControl('', Validators.required),
    number: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required),
    tariff: new FormControl('', Validators.required),
    mobile: new FormControl('', Validators.required),
    email: new FormControl(''),
    exten: new FormControl('')
  });

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.clientForm.invalid) {
      alert("Required");
      return;
    }
    
  }

}
